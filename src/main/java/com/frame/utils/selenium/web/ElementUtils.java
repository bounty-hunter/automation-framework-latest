package com.frame.utils.selenium.web;



import static com.frame.constants.Constants.LOG_DESIGN;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.frame.utils.common.Config;

/**
 * This class is responsible for performing all required user actions to
 * automate a web application. It is generally made for web applications that
 * run on desktop(e.g Windows/Mac etc.) browsers. Some methods might work for
 * mobile devices also but complete support is not present.
 *
 */
public class ElementUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElementUtils.class);
	public static WebDriver driver = null;

	
	public static void setDriver(WebDriver driver) {
		ElementUtils.driver = driver;
	}
	
	
	/**
	 *  It will refresh the current browser tab.
	 */
	public static void refreshBrowser() {
		LOGGER.info(LOG_DESIGN +"Refreshing the browser...");
		driver.navigate().refresh();
	}
	
	/**
	 *  It will refresh the current browser tab by getting its current URL.
	 */
	public static void refreshWithCurrentUrl() {
		LOGGER.info(LOG_DESIGN +"Refreshing the browser...");
		driver.get(driver.getCurrentUrl());
	}
	
	/**
	 *  It will refresh the current browser by hitting F5 key.
	 */
	public static void refreshWithF5Key() {
		LOGGER.info(LOG_DESIGN +"Refreshing the browser...");
		Actions actions = new Actions(driver);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.F5).perform();
	}
	
	/**
	 * It will return locator value present in or.properties file.
	 * 
	 * @param locatorName
	 * @return locator value
	 */
	public static String getLocatorValue(String locatorName) {
		return Config.getProperty(locatorName);
	}
		
	/**
	 * It will get element.
	 * 
	 * @param locator
	 * @throws Exception
	 */
	public static WebElement getElement(String locator) {
		String locatorValue = Objects.isNull(Config.getProperty(locator))? locator : Config.getProperty(locator);
		LOGGER.info(LOG_DESIGN + "Getting element for :-- [{}] : [{}]", locator, locatorValue);
		WebElement element = null;
		try {
			element = driver.findElement(getByObject(locator.trim()));
		} catch (Exception e) {
			LOGGER.error(LOG_DESIGN + "!!!!!! Exception Occurred : {}: ", e.getMessage());
		}

		return element;
	}
	
	/**
	 * It will get list of elements of a specific locator.
	 * 
	 * @param locator
	 * @throws Exception
	 */
	public static List<WebElement> getElements(String locator) {
		String locatorValue = Objects.isNull(Config.getProperty(locator))? locator : Config.getProperty(locator);
		LOGGER.info(LOG_DESIGN + "Getting elements for :-- [{}] : [{}]", locator, locatorValue);
		List<WebElement> elements = null;
		try {
			elements = driver.findElements(getByObject(locator.trim()));
		} catch (Exception e) {
			LOGGER.error(LOG_DESIGN + "!!!!!! Exception Occurred : {}: ", e.getMessage());
		}

		return elements;
	}
	
	/** It will give the object of "By" : 
	 * @param locator
	 * @return By object
	 */
	public static By getByObject(String locator) {
		String locatorValue = Objects.isNull(Config.getProperty(locator)) ? locator : Config.getProperty(locator).trim();
		By byObj = null;
		if (locator.endsWith("_xpath")) {
			byObj = By.xpath(locatorValue.replaceAll("_xpath", ""));
		} else if (locator.endsWith("_css")) {
			byObj = By.cssSelector(locatorValue.replaceAll("_css", ""));
		} else if (locator.endsWith("_id")) {
			byObj = By.id(locatorValue.replaceAll("_id", ""));
		} else if (locator.endsWith("__linkText")) {
			byObj = By.linkText(locatorValue.replaceAll("__linkText", ""));
		}
		
		return byObj;
		
	}
	
	/**  It just check that an element is present on the DOM of a page. 
	 * @param locator
	 * @param seconds
	 */
	public static void waitForElementPresence(String locator, long seconds) {
		LOGGER.info(LOG_DESIGN + "waiting for presence of element [{}] for {} seconds", locator, seconds);
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.presenceOfElementLocated(getByObject(locator)));
	}
	
	/**  It just check that an element is present on the DOM of a page. 
	 * @param locator
	 */
	public static void waitForElementPresence(String locator) {
		LOGGER.info(LOG_DESIGN + "waiting for presence of element [{}] for maxTimeout", locator);
		WebDriverWait wait = new WebDriverWait(driver, Long.valueOf(Config.getProperty("maxTimeOut")));
		wait.until(ExpectedConditions.presenceOfElementLocated(getByObject(locator)));
	}
	
	/** It will check that an element is present on the DOM of a page and visible. 
	 * @param locator
	 * @param seconds
	 */
	public static void waitForElementVisibility(String locator, long seconds) {
		LOGGER.info(LOG_DESIGN + "waiting for visibility of element [{}] for {} seconds", locator, seconds);
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getByObject(locator)));
	}
	
	/** It will check that an element is present on the DOM of a page and visible. 
	 * @param locator
	 */
	public static void waitForElementVisibility(String locator) {
		LOGGER.info(LOG_DESIGN + "waiting for visibility of element [{}] for maxTimeout", locator);
		WebDriverWait wait = new WebDriverWait(driver, Long.valueOf(Config.getProperty("maxTimeOut")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(getByObject(locator)));
	}
	
	/**
	 * It will click on a given locator.
	 * 
	 * @param locator
	 */
	public static void click(String locator) {
		try {
			LOGGER.info(LOG_DESIGN + "Clicking on : [{}] : [{}]", locator, Config.getProperty(locator));
			WebElement element = getElement(locator);
			highlightWebElement(element);
			element.click();
		} catch (Exception e) {
			LOGGER.error(LOG_DESIGN + "Exception occurred while clicking : [{}]", e.getMessage());
		}
	}
	
	/**
	 * It will click on a given locator.
	 * 
	 * @param locator
	 */
	public static void click(WebElement element) {
		try {
			LOGGER.info(LOG_DESIGN + "Clicking on : [{}]", element);
			highlightWebElement(element);
			element.click();
		} catch (Exception e) {
			LOGGER.error(LOG_DESIGN + "Exception occurred while clicking : [{}]", e.getMessage());
		}
	}

	/** It will navigate to the specified URL.
	 * @param URL
	 */
	public static void navigateToURL(String URL) {
		LOGGER.info(LOG_DESIGN + "Navigating to URL : [{}]", URL);
		driver.navigate().to(URL);
		
	}
	
	/** It will give the page title.
	 * @return
	 */
	public static String getPageTitle() {
		String pageTitle  = driver.getTitle();
		LOGGER.info(LOG_DESIGN + "Page Title is : [{}]", pageTitle);
		
		return pageTitle;
		
	}
	
	/** It will get an element's text using any attribute.
	 * For e.g attribute can be : innerText,textContent,title etc as defined in the element html code
	 * @param locator
	 * @param attribute
	 * @return
	 */
	public static String getTextUsingAttribute(String locator, String attribute) {
		WebElement element = getElement(locator);
		highlightWebElement(element);
		String elementText = element.getAttribute(attribute);
		LOGGER.info(LOG_DESIGN + "Text found for : [{}] is : [{}]", locator, elementText);
		
		return elementText;
		
	}
	
	/** It enters the value in text box.
	 * @param locator
	 * @param text
	 */
	public static void enterText(String locator, String text) {
		WebElement element = getElement(locator);
		highlightWebElement(element);
		LOGGER.info(LOG_DESIGN + "Entering text for : [{}] ::   Text is : [{}]", locator, text);
		element.sendKeys(text);
		
	}
	

	/** It enters the value in text box.
	 * @param locator
	 * @param text
	 */
	public static void enterTextUsingActions(String locator, String text) {
		WebElement element = getElement(locator);
		highlightWebElement(element);
		LOGGER.info(LOG_DESIGN  + "Entering text for : [{}] ::   Text is : [{}]", locator, text);
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.click();
		actions.sendKeys(text);
		actions.build().perform();
		
	}
	
    /**
     * It focuses on given web element.
     * 
     * @param driver
     * @param element
     */
    public static void jsFocus(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].focus();", element);
    }
    
    /**
     * It clicks on given web element using javascript.
     * 
     * @param element
     */
    public static void jsClick(WebElement element) {
    	LOGGER.info(LOG_DESIGN + "Clicking on element : {} using javascript", element);
    	highlightWebElement(element);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }
    
    /**
     * It clicks on given locator using javascript.
     * 
     * @param element
     */
    public static void jsClick(String locator) {
    	LOGGER.info(LOG_DESIGN + "Clicking on locator : {} using javascript", locator);
    	WebElement element = getElement(locator);
        highlightWebElement(element);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }
    
	/**
	 * It scrolls to the given WebElement.
	 * 
	 * @param driver
	 * @param element
	 * @return WebElement
	 */
    public static WebElement scrollingToElementofAPage(WebElement element) {
    	LOGGER.info(LOG_DESIGN + "Scrolling to element : {} ", element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
       
        return element;
    }
    
    /** It will highlight the web element
     * @param element
     */
    public static void highlightWebElement(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('style', 'background:#ffffb3; border:3px solid green;');", element);
    }
    
	/**
	 * This method copies the content to system Clipboard and than paste it. i.e it
	 * just performs copy paste operation like (Ctrl C + Ctrl V)
	 * 
	 * @param element
	 * @param stringToBePasted
	 */
	public static void copyPaste(WebElement element, String stringToBePasted) {
		LOGGER.info(LOG_DESIGN+ "Clearing any text if present..");
		element.clear();
		element.click();
		LOGGER.info(LOG_DESIGN + "copy pasting : [{}] .", stringToBePasted);
		StringSelection stringSelection = new StringSelection(stringToBePasted);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		} catch (AWTException e) {
			LOGGER.error(LOG_DESIGN + "!!!!!!!! Exception occured while copy pasting the given content....: {}", e.getMessage());
		}
		
	}
	
	/** It will click on element using Actions class.
	 * @param element
	 * @param seconds
	 */
	public static void actionClick(WebElement element, long seconds) {
			WebDriverWait driverWait = new WebDriverWait(driver, seconds);
			driverWait.until(ExpectedConditions.elementToBeClickable(element));
			Actions actions = new Actions(driver);
			actions.click(element).build().perform();
		    }
	
	/** It will click on element using Actions class.
	 * @param element WebElement
	 */
	public static void actionClick(WebElement element) {
			WebDriverWait driverWait = new WebDriverWait(driver, Long.valueOf(Config.getProperty("maxTimeOut")));
			driverWait.until(ExpectedConditions.elementToBeClickable(element));
			Actions actions = new Actions(driver);
			actions.click(element).build().perform();
		    }
}
