package com.frame.utils.selenium.driver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.frame.utils.common.Config;
import static com.frame.constants.Constants.*;

/**
 * It contains pool of web drivers for mobile devices.
 *
 */
public class MobileDriverPool {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MobileDriverPool.class);

	/** It will return driver for mobile chrome browser.
	 * @param nodeURL
	 * @return driver for mobile chrome browser.
	 * @throws MalformedURLException
	 */
	public static WebDriver getAndroidChromeDriver(String nodeURL) throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("browserName", "chrome");
		capabilities.setCapability("deviceName", Config.getProperty(String.valueOf("deviceName")));
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appActivity", "com.google.android.apps.chrome.Main");

		LOGGER.info(LOG_DESIGN +"Launching android chrome driver with capabilities : {}", capabilities);
		
		return new RemoteWebDriver(new URL(nodeURL), capabilities);
	}

}
