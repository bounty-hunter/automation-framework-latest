package com.frame.utils.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.frame.reporter.ExtentReporter;

/** It contains all methods for assertion or validation with added log info.
 *
 */
public class CustomAssertion {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomAssertion.class);

	
	/** It will make assertion of two strings.
	 * @param actual
	 * @param expected
	 */
	public static void assertEquals(final String actual, final String expected) {
		LOGGER.info("Validating :- Actual [{}] : " + "Expected [{}]", actual, expected);
		Assert.assertEquals(actual, expected);
	}

	
	/** It will make assertion of two strings.
	 * @param actual
	 * @param expected
	 */
	public static void assertEquals(final String actual, final String expected, final String message) {
		LOGGER.info("Validating :- Actual [{}] : " + "Expected [{}]", actual, expected);
		Assert.assertEquals(actual, expected);
		ExtentReporter.info(message);
	}

}
