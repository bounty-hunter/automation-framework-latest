package com.frame.utils.common;
import static com.frame.constants.Constants.LOG_DESIGN;
import static com.frame.constants.Constants.SUITE_XML_PATH;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * It contains utility methods for performing file related operations (e.g
 * create,read etc.).
 *
 */
public class FileUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

	/**
	 * It creates directory or folder at provided path
	 * 
	 * @param directoryPath
	 */
	public static void createDirectory(String directoryPath) {
		LOGGER.info(LOG_DESIGN + "Creating directory : {}", directoryPath);
		(new File(directoryPath)).mkdirs();
	}
	
	/** It will generate a xml file with the provided xml string.
	 * @param file complete file name e.g src//main//resources//dynamicXml.xml i.e where we want to create the file along with its name.
	 * @param xmlString
	 */
	public static void generateXmlFile(String file, String xmlString) {
		try {
			FileWriter fw = new FileWriter(file);
			fw.write(xmlString);
			fw.close();
			LOGGER.info(LOG_DESIGN + "Successfully generated XML file : {}", file);
		} catch (IOException e) {
			LOGGER.error(LOG_DESIGN + "!!!! Exception occurred while copying xml string to file : {}", e.getMessage());
		}
	}


}
